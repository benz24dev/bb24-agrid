<?php

namespace Bb24\Agrid;

use Illuminate\Support\ServiceProvider;

class AgridServiceProvider extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		$pkg_path 	= dirname(__DIR__);

		$views_path = __DIR__ . '/../resources/views';
		$this->loadViewsFrom( $views_path, 'agrid' );

		$this->publishes([
			$pkg_path . '/resources/assets/js' => public_path('vendor/bb24/agrid/js'),
		], 'agrid');
	}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadRoutesFrom( __DIR__ . DIRECTORY_SEPARATOR . 'web.php' );
	}
}
