<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 18.07.2019
 * Time: 08:56
 */

namespace Bb24\Agrid;


use Bb24\Agrid\SelectedFieldConfig;
use Nayjest\Grids\Components\Base\TComponent;
use Nayjest\Grids\Components\Base\TRegistry;
use Nayjest\Grids\DataProvider;
use Nayjest\Grids\SelectFilterConfig;


class SelectableConfig {

	use TRegistry;
	use TComponent;

	const FILTER_CHECKED					= 'checked';
	const FILTER_UNCHECKED					= 'unchecked';

	protected $onChange						= '';

	protected $sSelectableFieldName			= 'id';

	protected $selectableFieldConfig		= null;

	protected $renderSelectCurrent			= true;

	protected $renderSelectAll				= true;

	protected $renderSelectableDropdown		= true;

	protected $selectedEntities				= array();

	protected $defaultFilter				= null;



	/**
	 * @return null
	 */
	public function getDefaultFilter() {

		return $this->defaultFilter;
	}



	/**
	 * @param null $defaultFilter
	 */
	public function setDefaultFilter( $defaultFilter ): self {

		$this->defaultFilter = $defaultFilter;
		return $this;
	}



	/**
	 * @return array
	 */
	public function getSelectedEntities(): array {

		return $this->selectedEntities;
	}



	/**
	 * @param array $selectedEntities
	 */
	public function setSelectedEntities( $selectedEntities=array() ): self {

		$this->selectedEntities = $selectedEntities;
		return $this;
	}




	/**
	 * @return bool
	 */
	public function isRenderSelectableDropdown(): bool {

		return $this->renderSelectableDropdown;
	}



	/**
	 * @param bool $renderSelectDropdown
	 */
	public function setRenderSelectableDropdown( bool $renderSelectableDropdown ): self {

		$this->renderSelectableDropdown = $renderSelectableDropdown;
		return $this;
	}







	/**
	 * @return bool
	 */
	public function isRenderSelectCurrent(): bool {

		return $this->renderSelectCurrent;
	}



	/**
	 * @param bool $renderSelectCurrent
	 */
	public function setRenderSelectCurrent( bool $renderSelectCurrent ): self {

		$this->renderSelectCurrent = $renderSelectCurrent;
		return $this;
	}



	/**
	 * @return bool
	 */
	public function isRenderSelectAll(): bool {

		return $this->renderSelectAll;
	}



	/**
	 * @param bool $renderSelectAll
	 */
	public function setRenderSelectAll( bool $renderSelectAll ): self {

		$this->renderSelectAll = $renderSelectAll;
		return $this;
	}




	/**
	 * @return string
	 */
	public function getOnChange(): string {

		return $this->onChange;
	}



	/**
	 * @param string $onChange
	 */
	public function setOnChange( string $onChange ): self {

		$this->onChange = $onChange;
		return $this;
	}



	/**
	 * @return string
	 */
	public function getSelectableFieldName(): string {

		return $this->sSelectableFieldName;
	}



	/**
	 * @param string $sSelectableFieldName
	 */
	public function setSelectableFieldName( string $sSelectableFieldName ): self {

		$this->sSelectableFieldName = $sSelectableFieldName;
		return $this;
	}



	/**
	 * @return null
	 */
	public function getSelectableFieldConfig() {

		return $this->selectableFieldConfig;
	}



	/**
	 * @param null $selectableFieldConfig
	 */
	public function setSelectableFieldConfig( $selectableFieldConfig ): self {

		$this->selectableFieldConfig = $selectableFieldConfig;
		return $this;
	}






}