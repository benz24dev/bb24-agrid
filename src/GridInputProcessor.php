<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 09.07.2019
 * Time: 08:53
 */

namespace Bb24\Agrid;


use Nayjest\Grids\FieldConfig;
use Form;

class GridInputProcessor extends \Nayjest\Grids\GridInputProcessor {


	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param array $new_params
	 *
	 * @return  string
	 */
	public function getQueryString( array $new_params = [] ) {
		$params = $_GET;
		if (!empty($this->input)) {
			$params[$this->getKey()] = $this->input;
		}
		if (!empty($new_params)) {
			if (empty($params[$this->getKey()])) {
				$params[$this->getKey()] = [];
			}
			foreach ($new_params as $key => $value) {
				$params[$this->getKey()][$key] = $value;
			}
		}
		return http_build_query($params);
	}




	/**
	 * @param FieldConfig $column
	 * @param $direction
	 * @return $this
	 */
	public function setSorting(FieldConfig $column, $direction)
	{
		$this->input['sort-col'] = $column->getName();
		$this->input['sort-dir'] = $direction;

		return $this;
	}



	public function getSortingHiddenInputsHtml()
	{
		$html = '';

		$key = $this->getKey();

		$html .= Form::hidden("{$key}[sort-col]", $this->input['sort-col'] ?? null, array( 'class' => 'hidden-sorting-col' ));
		$html .= Form::hidden("{$key}[sort-dir]", $this->input['sort-dir'] ?? null, array( 'class' => 'hidden-sorting-dir' ));

		return $html;
	}



	/**
	 * Returns sorting parameters passed to input.
	 *
	 * @return mixed
	 */
	public function getSorting()
	{

		dd( $this->input );
		return [$this->input[ 'sort-col' ] ?? null, $this->input[ 'sort-dir' ] ?? null];
	}
}