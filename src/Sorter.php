<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 09.07.2019
 * Time: 09:02
 */

namespace Bb24\Agrid;

use Nayjest\Grids\FieldConfig;
use Bb24\Agrid\GridInputProcessor;


class Sorter extends \Nayjest\Grids\Sorter {


	/**
	 * Returns URL for sorting control.
	 *
	 * @param FieldConfig $column
	 * @param $direction
	 * @return string
	 */
	public function link(FieldConfig $column, $direction)
	{
		return (new GridInputProcessor($this->grid))
			->setSorting($column, $direction)
			->getUrl();
	}



	/**
	 * Applies sorting to data provider.
	 */
	public function apply()
	{
		$input = $this->grid->getInputProcessor()->getInput();

		$sort = null;
		if (isset($input['sort-col']) && isset( $input[ 'sort-dir' ] ) ) {
			$sort	= [$input['sort-col'], $input['sort-dir']];
		}
		foreach ($this->grid->getConfig()->getColumns() as $column) {
			if ($sort) {
				if ($column->getName() === $sort[0]) {
					$column->setSorting($sort[1]);
				} else {
					$column->setSorting(null);
				}
			} else {
				if ($direction = $column->getSorting()) {
					$sort = [$column->getName(), $direction];
				}
			}
		}
		if ($sort) {
			$this
				->grid
				->getConfig()
				->getDataProvider()
				->orderBy($sort[0], $sort[1]);
		}
	}


}