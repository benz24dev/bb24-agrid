<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 01.07.2019
 * Time: 15:17
 */

namespace Bb24\Agrid\Components\Laravel5;


class Pager extends \Nayjest\Grids\Components\Laravel5\Pager {


	/**
	 * Renders pagination links & returns rendered html.
	 */
	protected function links()
	{
		/** @var  Paginator $paginator */
		$paginator = $this->grid->getConfig()
			->getDataProvider()
			->getPaginator();
		$input = $this->grid->getInputProcessor()->getInput();
		if (isset($input['page'])) {
			unset($input['page']);
		}

		$sTemplate	= null;
		if( $this->grid->getConfig()->isAjax() ) {
			$sTemplate	= 'agrid::bb24.pagination.default_ajax';
		}
		else {
			$sTemplate	= 'agrid::bb24.pagination.default';
		}

		return str_replace('/?', '?',$paginator->appends($this->input_key, $input)->render($sTemplate));
	}

}