<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 03.07.2019
 * Time: 09:05
 */

namespace Bb24\Agrid\Components;




use Nayjest\Grids\Grid;

class ColumnHeadersRow extends \Nayjest\Grids\Components\ColumnHeadersRow {


	/**
	 * Creates children components for rendering column headers.
	 *
	 * @param Grid $grid
	 */
	protected function createHeaders( Grid $grid)
	{
		foreach ($grid->getConfig()->getColumns() as $column) {
//			if( $grid->getConfig()->isAjax() ) {
				$mColumnHeader	= new \Bb24\Agrid\Components\ColumnHeader($column);
//			}
//			else {
//				$mColumnHeader	= new \Nayjest\Grids\Components\ColumnHeader($column);
//			}
			$this->addComponent( $mColumnHeader );
		}
	}

}