<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 03.07.2019
 * Time: 08:57
 */

namespace Bb24\Agrid\Components;


use Nayjest\Grids\FieldConfig;

class ColumnHeader extends \Nayjest\Grids\Components\ColumnHeader {


	/**
	 * @param FieldConfig $column
	 * @return $this
	 */
	public function setColumn(FieldConfig $column)
	{
		$this->setContent($column->getLabel());
		if ($column->isSortable() ) {
			$mSortingControl	= new \Bb24\Agrid\Components\SortingControl($column);

			$this->addComponent($mSortingControl);
		}

		$this->column = $column;
		return $this;
	}



}