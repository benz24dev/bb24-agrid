<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 01.07.2019
 * Time: 15:25
 */

namespace Bb24\Agrid\Components;


use Illuminate\Foundation\Application;
use Nayjest\Grids\Components\OneCellRow;

class TFoot extends \Nayjest\Grids\Components\TFoot {

	protected $gridConfig	= null;



	public function __construct( $gridConfig ) {

		$this->gridConfig	= $gridConfig;
	}



	/**
	 * Returns default set of child components.
	 *
	 * @return \Nayjest\Grids\Components\Base\ComponentInterface[]
	 */
	protected function getDefaultComponents()
	{
		if (version_compare(Application::VERSION, '5', '<')) {
			$pagerClass = 'Nayjest\Grids\Components\Pager';
		} else {
			$pagerClass = 'Bb24\Agrid\Components\Laravel5\Pager';
		}
		return [
			(new OneCellRow)
				->addComponent(new $pagerClass)
		];
	}
}