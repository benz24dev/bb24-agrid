<?php

namespace Bb24\Agrid\Components\Base;

use Nayjest\Grids\Components\Base\RenderableComponent;

/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 11.07.2019
 * Time: 10:41
 */


class RenderableView extends RenderableComponent {


	protected $arrViewData	= array();


	public function __construct( $sTemplate=null, $arrViewData=array() ) {
		$this->template	= $sTemplate;
		$this->arrViewData	= $arrViewData;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $arrData
	 */
	public function setViewData( $arrData ){
		$this->arrViewData	= $arrData;
	}



	/**
	 * Returns variables for usage inside view template.
	 *
	 * @return array
	 */
	protected function getViewData() {
		return $this->grid->getViewData() + [
			'component' => $this
		] + $this->arrViewData;
	}


}