<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 01.07.2019
 * Time: 15:25
 */

namespace Bb24\Agrid\Components;


use Illuminate\Foundation\Application;
use Nayjest\Grids\Components\OneCellRow;

class THead extends \Nayjest\Grids\Components\THead {

	protected $gridConfig	= null;


	public function __construct( $gridConfig ) {

		$this->gridConfig	= $gridConfig;
	}



	/**
	 * Returns default set of child components.
	 *
	 * @return \Nayjest\Grids\Components\Base\ComponentInterface[]
	 */
	protected function getDefaultComponents()
	{
		return [
			new ColumnHeadersRow(),
			($this->gridConfig->isAjax()) ? new FiltersRow() : new \Nayjest\Grids\Components\FiltersRow()
		];
	}





}