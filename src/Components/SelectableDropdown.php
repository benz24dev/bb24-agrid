<?php
namespace Bb24\Agrid\Components;

use Nayjest\Grids\Components\Base\RenderableComponent;

/**
 * Class RecordsPerPage
 *
 * The component renders control
 * for switching count of records displayed per page.
 *
 * @package Nayjest\Grids\Components
 */
class SelectableDropdown extends RenderableComponent
{

	protected $name = 'selectable_dropdown';

	protected $template = '*.components.selectable_dropdown';


	/**
	 * Returns variables for usage inside view template.
	 *
	 * @return array
	 */
	protected function getViewData()
	{
		return $this->grid->getViewData() + [
			'component' => $this,
			'config'	=> $this->grid->getConfig()
		];
	}


}
