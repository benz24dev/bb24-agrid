<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 22.07.2019
 * Time: 15:29
 */

namespace Bb24\Agrid\Components;


use Nayjest\Grids\Components\TableCell;

class Tr extends \Nayjest\Grids\Components\Tr {


	/**
	 * Renders row cells.
	 *
	 * @return string
	 */
	protected function renderCells()
	{
		$row = $this->getDataRow();
		$out = '';
		foreach($this->grid->getConfig()->getColumns() as $column) {
			$component = new TableCell($column);
			$component->initialize($this->grid);
			$component->setContent($column->getValue($row));
			if( $this->grid->getConfig()->isPureJs() ) {
				$arrAttributes	= array_merge( $component->getAttributes(), array( 'data-value' => $column->getValue($row) ));
				$component->setAttributes( $arrAttributes );
			}
			$out .= $component->render();
		}
		return $out;
	}

}