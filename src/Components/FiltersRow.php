<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 03.07.2019
 * Time: 11:45
 */

namespace Bb24\Agrid\Components;


class FiltersRow extends \Nayjest\Grids\Components\FiltersRow {


	const NAME 					= 'filters_row';
	protected $template 		= '*.components.filters_row';
	protected $name 			= FiltersRow::NAME;
	protected $render_section 	= self::SECTION_END;

}