<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 22.07.2019
 * Time: 14:35
 */

namespace Bb24\Agrid;



class EloquentDataProvider extends \Nayjest\Grids\EloquentDataProvider {

	protected $paginatorEnabled	= true;



	/**
	 * @return bool
	 */
	public function isPaginatorEnabled(): bool {

		return $this->paginatorEnabled;
	}



	/**
	 * @param bool $paginatorEnabled
	 */
	public function setPaginatorEnabled( bool $paginatorEnabled ): self {

		$this->paginatorEnabled = $paginatorEnabled;
		return $this;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  \Illuminate\Pagination\Paginator|mixed
	 */
	public function getPaginator() {

		if (!$this->paginator ) {
			if( !$this->isPaginatorEnabled() ) {
				$this->page_size = $this->src->count();
			}

			$this->paginator = $this->src->paginate($this->page_size);
		}



		return $this->paginator;
	}

}