<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 01.07.2019
 * Time: 10:35
 */

namespace Bb24\Agrid;


use Bb24\Agrid\Interfaces\EntitySelect;
use Html;
use Nayjest\Grids\FieldConfig;

class Grid extends \Nayjest\Grids\Grid {


	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  string
	 */
	public function renderRows(){
		$this->prepare();
		$provider = $this->config->getDataProvider();
		$provider->reset();

		$data	= $this->getViewData()[ 'data' ];
		$sRows	= '';
		while( $row = $data->getRow() ) {
			$sRows	.= $this->getConfig()->getRowComponent()->setDataRow( $row )->render();
		}

		return $sRows;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  int|string
	 */
	public function getSortingField(){
		if( ( $arrSort = $this->getInputProcessor()->getSorting() ) != null && is_array( $arrSort ) ) {
			return $arrSort[0];
		}

		return null;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  mixed
	 */
	public function getSortingOrder(){
		if( ( $arrSort = $this->getInputProcessor()->getSorting() ) != null && is_array( $arrSort ) ) {
			return $arrSort[1];
		}

		return null;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  \Illuminate\Support\Collection
	 */
	public function getSelectedModelsCollection( $blnFetch=false ){
		$this->getFiltering()->apply();
		$cModels	= $this->config->getDataProvider()->getBuilder();

		if( $this->config->getSelectMode() == EntitySelect::SELECT_ALL ) {
			$cModels	= $cModels->whereNotIn( $this->config->getSelectableConfig()->getSelectableFieldName(), array_values( $this->config->getSelectedEntities() ) );
		}
		else {
			$cModels	= $cModels->whereIn( $this->config->getSelectableConfig()->getSelectableFieldName(), array_values( $this->config->getSelectedEntities() ) );
		}

		if( $blnFetch ) return $cModels->get();

		return $cModels;
	}




	/**
	 * Sorts columns according to its order.
	 */
	protected function sortColumns()
	{
		$this->config->setColumns(
			$this->config->getColumns()->sort(function (FieldConfig $a, FieldConfig $b) {
				return $a->getOrder() > $b->getOrder();
			})
		);
	}



	/**
	 * Returns instance of GridInputProcessor.
	 *
	 * @return GridInputProcessor
	 */
	public function getInputProcessor()
	{
		if (null === $this->input_processor) {
			$this->input_processor	= new GridInputProcessor($this);
		}
		return $this->input_processor;
	}



	/**
	 * Returns data sorting manager.
	 *
	 * @return Sorter
	 */
	public function getSorter()
	{
		if (null === $this->sorter) {
			$this->sorter = new Sorter($this);
		}
		return $this->sorter;
	}
}