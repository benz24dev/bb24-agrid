<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 11.07.2019
 * Time: 14:16
 */

namespace Bb24\Agrid\Traits;




use Illuminate\Http\Request;

trait TMassUpdate {

	public function massupdate( Request $request ){
		$action	= $request->get( 'massupdate-action' );
		$mGrid	= $this->loadGrid( $request->get( 'grid-name' ) );

		$arrRouteParts	= explode( '@', $action );
		$mController	= new $arrRouteParts[0]();
		$mController->{$arrRouteParts[1]}( $request, $mGrid );
	}

}