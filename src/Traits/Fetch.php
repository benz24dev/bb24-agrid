<?php

namespace Bb24\Agrid\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait Fetch {


	/**
	 * Rendert das Grid 
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param Request $request
	 *
	 * @throws \Exception
	 */
	public function fetch(Request $request){
		$mGrid	= $this->loadGrid( $request->get( 'grid-name' ) );
		
		return $mGrid->render();
	}



	/**
	 * Lädt das Grid
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $sGridName
	 *
	 * @return  mixed
	 * @throws \Exception
	 */
	public function loadGrid( $sGridName ){
		$sFunctionName	= Str::camel( $sGridName );
		if(!method_exists( $this, $sFunctionName )){
			throw new \Exception( 'Die Funktion \'' . $sFunctionName . '\' für das Grid \'' . $sGridName . '\' existiert nicht.' );
		}

		return self::{$sFunctionName}();
	}



}
