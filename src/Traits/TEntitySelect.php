<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 08.07.2019
 * Time: 09:42
 */

namespace Bb24\Agrid\Traits;

use App\Models\ArticleFlat;
use Bb24\Agrid\Components\ButtonRow;
use Bb24\Agrid\Components\SelectableDropdown;
use Bb24\Agrid\Components\THead;
use Bb24\Agrid\Interfaces\EntitySelect;
use Bb24\Agrid\SelectableConfig;
use Bb24\Agrid\SelectedFieldConfig;
use Nayjest\Grids\DataProvider;
use Nayjest\Grids\FieldConfig;
use Html;
use Illuminate\Support\Arr;
use Nayjest\Grids\SelectFilterConfig;


trait TEntitySelect{

	/**
	 * @var null|SelectableConfig $selectableConfig
	 */
	protected $selectableConfig			= null;

	protected $arrSelectedEntities		= null;

	protected $sSelectedEntities		= null;

	protected $sSelectMode				= null;



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  SelectableConfig|null
	 */
	public function getSelectableConfig(): ?SelectableConfig{

		return $this->selectableConfig;
	}




	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  \Illuminate\Pagination\Paginator|int
	 */
	public function getSelectedCount(){
		$iCount 				= 0;
		$arrSelectedEntities	= $this->getSelectedEntities();

		$query 					= $this->getDataProvider()->getBuilder()->getQuery();

		if( $this->getSelectMode() != EntitySelect::SELECT_ALL && count( $arrSelectedEntities ) == 0 ) return 0;

		if( $this->getSelectMode() == EntitySelect::SELECT_ALL ) {
			if( count( $arrSelectedEntities ) > 0 ) {
				$query->limit(null)->whereNotIn(
					$this->getSelectableConfig()->getSelectableFieldName(),
					array_values( $arrSelectedEntities )
				);

				$query->offset = null;
			}
		}
		else if( count( $arrSelectedEntities ) > 0 ) {
			$query->limit(null)->whereIn(
				$this->getSelectableConfig()->getSelectableFieldName(),
				array_values( $arrSelectedEntities )
			);

			$query->offset = null;
		}

		$iCount	= $query->count();

		return $iCount;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  mixed
	 */
	public function getSelectMode(){
		if( isset( $this->sSelectMode ) == false ) {
			$this->sSelectMode	= Arr::get( request($this->name), 'entity-select-mode', EntitySelect::SELECT_NOTHING );
		}

		return $this->sSelectMode;
	}







	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  null
	 */
	public function getSelectedString(){
		if( isset( $this->sSelectedEntities ) == false ) {
			$this->sSelectedEntities	= Arr::get( request($this->name), 'selected-entities', '' );
		}

		return $this->sSelectedEntities;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  string
	 */
	public function getSelectedEntitiesAsString(){
		return implode( ',', $this->getSelectedEntities() );
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  array|false|\Illuminate\Http\Request|string
	 */
	public function getSelectedEntities(){
		if( isset( $this->arrSelectedEntities ) == false ) {
			$sSelectedEntitites				= $this->getSelectedString();
			$this->arrSelectedEntities		= explode( ',', $this->sSelectedEntities );
			$this->arrSelectedEntities		= array_filter( $this->arrSelectedEntities );

			$preSelectedEntities			= $this->getSelectableConfig()->getSelectedEntities();
			$this->arrSelectedEntities		= array_unique( array_merge( $preSelectedEntities, $this->arrSelectedEntities ) );

			if( count( $this->arrSelectedEntities ) > 0 ) {
				$query 						= clone $this->getDataProvider()->getBuilder()->getQuery();
				$query->limit(null)->offset	= null;

				$this->arrSelectedEntities	= $query->whereIn( $this->getSelectableConfig()->getSelectableFieldName(), $this->arrSelectedEntities )
					->pluck( $this->getSelectableConfig()->getSelectableFieldName() )
					->toArray();
			}

			$this->arrSelectedEntities	= array_combine( $this->arrSelectedEntities, $this->arrSelectedEntities );
		}

		return $this->arrSelectedEntities;
	}



	/**
	 *
	 * @author     l.brinkmann
	 * @since      master
	 * @version    master
	 *
	 * @param $blnSelectable
	 *
	 * @return GridConfig
	 */
	public function setSelectable( $selectableConfig ) {
		$this->selectableConfig	= ( $selectableConfig == false ) ? null : $selectableConfig;

		if( $this->isSelectable() ) {
			$this->getSelectableConfig()->attachTo( $this );

			if( $this->getSelectableConfig()->isRenderSelectableDropdown() == true ) {
				$this->getComponentByName( THead::NAME )
					->addComponent( (new ButtonRow())
						->setComponents([
							(new SelectableDropdown())
						])
						->setRenderSection( THead::SECTION_BEGIN )
					);
			}

			$this->setColumns( $this->getColumns()->toArray() );
		}
		else {
			$buttonRow	= $this->getComponentByName( THead::NAME )
				->getComponentByName( ButtonRow::NAME );

			$components	= $this->getComponentByName( THead::NAME )->getComponents();
			foreach( $components as $index => $component ) {
				if( $component == $buttonRow ) {
					unset( $components[ $index ] );
				}
			}

			$this->getComponentByName( THead::NAME )->setComponents( $components );

			$this->setColumns( $this->getColumns()->toArray() );
		}

		return $this;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @return  bool
	 */
	public function isSelectable() {
		return $this->selectableConfig instanceof SelectableConfig;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $iEntity
	 *
	 * @return  bool
	 */
	public function isEntitySelected( $iEntity ){
		$arrSelected	= $this->getSelectedEntities();

		if( $this->getSelectMode() == self::SELECT_ALL ) {
			$isSelected		= !isset( $arrSelected[ $iEntity ] );
		}
		else if( $this->getSelectMode() == self::SELECT_NOTHING ) {
			$isSelected		= isset( $arrSelected[ $iEntity ] );
		}

		return $isSelected;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $columns
	 *
	 * @return  mixed
	 */
	public function removeSelectableField( $columns ) {
		foreach( $columns as $key => $mColField ) {
			if( method_exists( $mColField, 'getIdentifier' )
				&& $mColField->getIdentifier() == SelectedFieldConfig::IDENTIFIER ) {

				unset( $columns[ $key ] );
			}
		}

		return $columns;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $columns
	 * @param $mField
	 *
	 * @return  array
	 */
	public function insertField( $columns, $mField ) {
		if( ( $index = $this->fieldIsSet( $columns, $mField ) ) !== false ) {
			$columns[ $index ]	= $mField;
		}
		else {
			$columns	= array_merge( array( $mField ), $columns );
		}

		return $columns;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 * @param $column
	 * @param $mField
	 *
	 * @return  bool|int
	 */
	public function fieldIsSet( $column, $mField ){
		foreach( $column as $key => $mColField ) {
			if( method_exists( $mColField, 'getIdentifier' )
				&& $mColField->getIdentifier() == $mField->getIdentifier() ){

				return $key;
			}
		}

		return false;
	}



	/**
	 *
	 * @author  l.brinkmann
	 * @since	master
	 * @version	master
	 *
	 */
	public function getSelectableField(){

		$selectFilterConfig		= (new SelectFilterConfig())
			->setName( 'selected_filter' )
			->setTemplate( '*.select_selected' )
			->setSubmittedOnChange( true )
			->setFilteringFunc( function( $val, DataProvider $provider ) {
				$arrSelected	= $this->getSelectedEntities();
				$arrSelected	= array_values( $arrSelected );
				if( $val == SelectableConfig::FILTER_CHECKED ) {
					if( $this->getSelectMode() == self::SELECT_ALL ) {
						$provider->getBuilder()->whereNotIn( $this->getSelectableConfig()->getSelectableFieldName(), $arrSelected );
					}
					else if( $this->getSelectMode() == self::SELECT_NOTHING ) {
						$provider->getBuilder()->whereIn( $this->getSelectableConfig()->getSelectableFieldName(), $arrSelected );
					}
				}
				else if( $val == SelectableConfig::FILTER_UNCHECKED ){
					if( $this->getSelectMode() == self::SELECT_ALL ) {
						$provider->getBuilder()->whereIn( $this->getSelectableConfig()->getSelectableFieldName(), $arrSelected );
					}
					else if( $this->getSelectMode() == self::SELECT_NOTHING ) {
						$provider->getBuilder()->whereNotIn( $this->getSelectableConfig()->getSelectableFieldName(), $arrSelected );
					}
				}
			});

		if( ( $defaultValue	= $this->getSelectableConfig()->getDefaultFilter() ) != null ) {
			$selectFilterConfig->setDefaultValue( $defaultValue );
		}


		$this->selectableFieldConfig	= (new SelectedFieldConfig())
			->setFilters( [
				$selectFilterConfig
			] )
			->setCallback( function( $val, $row ) {
				if( ( $onChange = $this->getSelectableConfig()->getOnChange() ) != '' ) {
					$onChange	= $onChange . '(event, this);';
				}

				$sContent	= \Html::tag( 'div',
					\Html::tag('input', '', array(
						'class'		=> 'form-check-input entity-select',
						'data-id'	=> $row->getSrc()->id,
						'onchange'	=> 'AGrid.handleCheckboxChange(this);' . $onChange,
						'type'		=> 'checkbox',
						'checked'	=> $this->isEntitySelected( $row->getSrc()->id )
					) )->toHtml(),
					array(
						'class'	=> 'form-check checkbox'
					) );

				return $sContent;
			} );

		return $this->selectableFieldConfig;
	}



}