<?php
/**
 * Created by PhpStorm.
 * User: l.brinkmann
 * Date: 17.07.2019
 * Time: 10:15
 */

namespace Bb24\Agrid;

use Nayjest\Grids\FieldConfig;

class SelectedFieldConfig extends FieldConfig {

	const IDENTIFIER		= 'selected_field';

	protected $identifier	= self::IDENTIFIER;



	/**
	 * @return string
	 */
	public function getIdentifier(): string {

		return $this->identifier;
	}



	/**
	 * @param string $identifier
	 */
	public function setIdentifier( string $identifier ): void {

		$this->identifier = $identifier;
	}






}