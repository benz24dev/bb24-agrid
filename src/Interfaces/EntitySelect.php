<?php

namespace Bb24\Agrid\Interfaces;

interface EntitySelect {
	const SELECT_ALL		= 'all';
	const SELECT_CURRENT	= 'current';
	const SELECT_NOTHING	= 'nothing';
}