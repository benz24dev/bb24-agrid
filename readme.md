# Bb24\Agrid
## Voraussetzungen
* php >= 5.4.0
* laravel >= 5.5
* nayjest/grid >= 1.3.1
## Setup
* *composer require Bb24/Agrid*
* *php artisan vendor:publish --tag=agrid*
* */public/vendor/bb24/agrid/js/AGrid.js* einbinden
## Funktionsweise
* Die Klassen *Bb24\Agrid\Config* und *Bb24\Agrid\GridConfig* sollten zum INitialisieren des Grids genutzt werden. 
Sie bieten z.B. die Möglichkeit das Grid via Ajax zu nutzen oder die Datensätze für das Massenupdate zu aktivieren.
* Die Ajax-Requests werden an die url **/agrid/fetch** gesendet. diese Anfrage landet standardmäßig in Bb24\Agrid\Controllers\FetchController. Die Routen können aber neu definiert werden und auf jeden Controller geroutet werden, der das Trait **Bb24\Agrid\Traits\Fetch** implementiert. ![Alternativtext](routes.PNG) 
In diesem Controller muss eine Funktion existieren, deren Namen dem Namen des Grids in Camel-Case entspricht. Dadurch wird beim Fetch die jeweilige Methode dynamisch aufgerufen.
![Alternativtext](fetch.PNG)
* Beim Massupdate muss eine onclick-Action hinzugefügt werden: **onclick="AGrid.massupdate( event, {test:123}, function(){} )"**
	* Parameter 1: event
	* Parameter 2: optionale Parameter
	* Parameter 3: callback funktion
* über das data-Attribut **data-action=""** muss die Action angegeben werden, die für das Massenupdate zuständig ist. ![Collection Laden](onclick.PNG) 
* In der Action kann dann das Grid ganz einfach geladen werden: ![Collection Laden](getCollection.PNG) 


