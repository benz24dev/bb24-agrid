AGrid	= function(){

};


jQuery( document ).on( 'change', 'thead .form-control', function(){
	var grid 	= AGrid.loadGridByElement( this );

	grid.changePage(1);
});


AGrid.grids	= {};



AGrid.addGrid	= function( identifier ){
	// if( typeof AGrid.grids[ identifier ] == 'undefined' ) {
	AGrid.grids[ identifier ]	= new AGrid.Grid( identifier );
	// }
};


AGrid.MODE_ALL		= 'all';
AGrid.MODE_NOTHING	= 'nothing';
AGrid.MODE_CURRENT	= 'current';



AGrid.Grid 	= function( identifier ){
	var self			= this;
	this.identifier 	= identifier;
	this.form			= undefined;
	this.mode 			= undefined;
	this.entityIds		= {};
	this.totalEntities	= undefined;




	this.changeButtonLabel		= function() {
		var button	= self.form.find( '.select-entity-button' );
		if( button.length > 0 ) {
			var iCount	= self.getSelectedCount();
			var content	= button.html();

			content 	= content.replace( /\([0-9]+\)/g, '(' + iCount + ')' );
			button.html( content );
		}
	};



	this.getSelectedCount		= function(){
		if( self.mode == AGrid.MODE_NOTHING ) {
			return Object.keys( self.entityIds ).length;
		}
		else {
			return self.totalEntities - Object.keys( self.entityIds ).length;
		}
	};



	this.select			= function(arrIds){
		for( var i in arrIds ) {
			if( self.mode == AGrid.MODE_ALL ) {
				delete self.entityIds[ arrIds[ i ] ];
			}
			else {
				self.entityIds[ arrIds[ i ] ]	= arrIds[ i ];
			}
		}

		this.updateEntityField();

		this.changeButtonLabel();
	};



	this.deSelect		= function(arrIds){
		for( var i in arrIds ) {
			if( self.mode == AGrid.MODE_ALL ) {
				self.entityIds[ arrIds[ i ] ]	= arrIds[ i ];
			}
			else {
				delete self.entityIds[ arrIds[ i ] ];
			}
		}

		this.updateEntityField();

		this.changeButtonLabel();
	};


	this.updateEntityField	= function(){
		var val	= Object.values( self.entityIds ).join( ',' );
		self.form.find( '.entity-select-ids' ).val( val );
	};


	this.switchMode 		= function( mode ){
		var input	= self.form.find( '.entity-select-mode' );

		var oldMode	= self.mode;
		self.mode 	= mode;

		if( mode == AGrid.MODE_ALL ) {
			self.entityIds	= {};
			self.form.find( 'input.entity-select' ).prop('checked', true).trigger('change');
		}
		else if( mode == AGrid.MODE_CURRENT ) {

			if( oldMode == AGrid.MODE_ALL ) {
				self.entityIds	= {};
			}

			self.mode	= AGrid.MODE_NOTHING;
			self.form.find( 'input.entity-select' ).prop('checked', true).trigger('change');
		}
		else if( mode == AGrid.MODE_NOTHING ) {
			self.entityIds	= {};
			self.form.find( 'input.entity-select' ).prop('checked', false).trigger('change');
		}

		input.val( self.mode );
	};


	this.sort				= function( column, order ){
		var input	= self.form.find( '.hidden-sorting-col' );
		input.prop( 'disabled', false );
		input.val( column );

		input	= self.form.find( '.hidden-sorting-dir' );
		input.prop( 'disabled', false );
		input.val( order );

		self.sorted		= true;

		self.changePage( 1 );
	};


	this.submit 			= function() {
		self.form.submit();
	};


	this.changePage			= function( page ) {
		self.form.find( '.grid-page' ).val( page );

		self.paged		= true;
	};


	this.massupdate			= function( event, params, callback ){
		var button		= jQuery( event.target ).closest( 'button' ).get( 0 );
		var action		= jQuery( button ).attr( 'data-action' );
		var dataParams	= {};
		if( typeof callback == 'undefined' ) callback = function(){};
		if( typeof params == 'undefined' ) params = {};

		var arrFields		= self.form.serializeJSON();

		jQuery( button.attributes ).each(function() {
			if( this.nodeName.includes( 'data-param-' ) == true ) {
				var key	= this.nodeName.substring( 11 );
				dataParams[ key ]	= this.nodeValue;
			}
		});

		var arrParams	= {
			'grid-name':			self.identifier,
			'massupdate-action':	action,
		};
		arrParams	= $.extend( arrParams, dataParams );
		arrParams	= $.extend( arrParams, params );

		arrParams[ self.identifier ]	= {
			'entity-select-mode':	self.mode,
			'selected-entities':	Object.values( self.entityIds ).join( ',' )
		};

		arrParams	= $.extend( arrParams, arrFields );

		$.ajax({
			url:	'/agrid/massupdate',
			data:	arrParams,
			success:	function( data ) {
				callback( data );
			}
		});
	};


	this.submitAjax			= function( params ){
		Ajaxloader.show();
		var arrFields		= self.form.serializeJSON();

		if( typeof params != 'undefined' ) {
			if( typeof params[ 'sort-col' ] != 'undefined' && typeof params[ 'sort-dir' ] != 'undefined' ) {
				arrFields[ self.identifier ][ 'sort-col' ] = params[ 'sort-col' ];
				arrFields[ self.identifier ][ 'sort-dir' ] = params[ 'sort-dir' ];
			}
		}

		var arrParams	= {
			'grid-name':	self.identifier
		};
		arrParams[ self.identifier ]	= params;

		$.extend( true, arrFields, arrParams );
		var url					= '/agrid/fetch';

		jQuery.ajax({
			url:		url,
			type:		'GET',
			data:		arrFields,
			success:	function( data ){
				// window.history.pushState( {}, '', currentFilterUri );
				self.form.replaceWith( data )
			},
			complete:	function(){
				self.form = jQuery( '.agrid-form[data-grid-name=' + self.identifier + ']' );
				Ajaxloader.hide();
			}
		})
	};



	this.filterRows			= function(){
		var filteredRows	= self.form.find( 'tbody tr' ).toArray();
		var filteringActive	= false;
		self.form.find( '.filter-input' ).each( function(){
			var input		= jQuery( this );
			var operator	= input.attr( 'data-operator' ).trim();
			var colClass	= input.closest( 'td' ).attr( 'class' );
			var pattern		= input.val().trim();

			if( pattern != undefined && pattern != '' ) {
				if( input.get(0).nodeName.toLowerCase() == 'select' ) {
					pattern	= input.find( 'option[value=' + pattern + ']' ).html().trim();
				}

				filteringActive	= true;
				for( var i in filteredRows ) {
					var tr	= jQuery( filteredRows[ i ] );
					var col	= tr.find( 'td.' + colClass );
					if( ( operator == 'like' && col.html().search( pattern ) == -1 )
						|| ( operator != 'like' && col.html() != pattern ) ) {

						delete filteredRows[ i ];
					}
				}
			}
		});

		self.form.find( 'tbody tr' ).removeClass( 'd-none' );
		if( filteringActive ) {
			self.form.find( ' tbody tr' ).addClass( 'd-none' );
			for( var i in filteredRows ) {
				jQuery( filteredRows[ i ] ).removeClass( 'd-none' );
			}
		}

	};




	(function(){
		self.form			= jQuery( '.agrid-form[data-grid-name=' + identifier + ']' );
		self.mode			= self.form.find( '.entity-select-mode' ).val();
		self.totalEntities	= self.form.find( '.select-entity-button' ).data( 'total-entities' );

		var sEntityIds	= self.form.find( '.entity-select-ids' ).val();
		if( typeof sEntityIds != 'undefined' ) {
			var arrEntityIds	= sEntityIds.split( ',' );
			for( var i in arrEntityIds ) {
				var val	= arrEntityIds[ i ];
				if( val != '' ) {
					self.entityIds[ val ]	= val;
				}
			}
		}
	}())

};


AGrid.changePage		= function( uiElement, page ) {
	var grid 	= AGrid.loadGridByElement( uiElement );

	grid.changePage( page );
};



AGrid.handleCheckboxChange	= function( uiElement ){
	var grid	= AGrid.loadGridByElement( uiElement );

	var iEntity	= jQuery( uiElement ).attr( 'data-id' );

	if( jQuery( uiElement ).is( ':checked' ) ) {
		grid.select( [iEntity] );
	}
	else {
		grid.deSelect([iEntity]);
	}

};




AGrid.getGrid	= function( id ) {
	return AGrid.grids[id];
};



AGrid.loadGridByElement		= function( uiElement ) {
	var form			= jQuery( uiElement ).closest( '.agrid-form' );
	var id				= form.attr( 'data-grid-name' );
	var grid			= AGrid.getGrid( id );

	return grid;
};



AGrid.submitAjax 	= function(uiElement, params) {
	var grid	= AGrid.loadGridByElement( uiElement );

	grid.submitAjax( params );
};



AGrid.submit 		= function( uiElement ) {
	var grid	= AGrid.loadGridByElement( uiElement );

	grid.submit();
};



AGrid.sort 			= function( uiElement, column, order ){
	var grid	= AGrid.loadGridByElement( uiElement );

	grid.sort( column, order );
};




AGrid.onSelectModeChange	= function( uiElement ){
	var id		= jQuery( uiElement ).closest( '.agrid-form' ).attr( 'data-grid-name' );
	var mode 	= jQuery( uiElement ).attr( 'data-select-mode' );
	var grid	= AGrid.getGrid( id );

	grid.switchMode( mode )
};


AGrid.urlToArray 	= function (url) {
	var request = {};
	var pairs = url.substring(url.indexOf('?') + 1).split('&');
	for (var i = 0; i < pairs.length; i++) {
		if(!pairs[i])
			continue;
		var pair = pairs[i].split('=');
		request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
	}
	return request;
};



AGrid.massupdate		= function( event, params, callback ) {
	var grid 	= AGrid.loadGridByElement( event.target );

	grid.massupdate( event, params, callback );
};



AGrid.arrayToUrl 	= function(array) {
	var pairs = [];
	for (var key in array)
		if (array.hasOwnProperty(key))

			pairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(array[key]));
	return pairs.join('&');
};



AGrid.filterRows		= function( uiElement ){
	var grid 	= AGrid.loadGridByElement( event.target );

	grid.filterRows();
};
