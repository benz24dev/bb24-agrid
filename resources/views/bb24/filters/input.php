<?php
/** @var Nayjest\Grids\Filter $filter */
?>

<input
    class="form-control input-sm filter-input"
    data-operator="<?= $filter->getConfig()->getOperator() ?>"
    name="<?= $filter->getInputName() ?>"
    value="<?= $filter->getValue() ?>"
	<?php if( $filter->grid->getConfig()->isPureJs() ): ?>
        onkeydown="if(event.keyCode == 13) {event.preventDefault();AGrid.filterRows(this);}"
    <?php elseif( $filter->grid->getConfig()->isAjax() ): ?>
        onkeydown="if(event.keyCode == 13) {event.preventDefault();AGrid.submitAjax(this);}"
	<?php endif; ?>
    />
<?php if($label): ?>
    <span><?= $label ?></span>
<?php endif ?>
