<?php
/** @var Nayjest\Grids\Filter $filter */
/** @var Nayjest\Grids\SelectFilterConfig $cfg */
$cfg = $filter->getConfig();
$onchange = '';

//dd($filter->grid->getConfig());
if( $filter->grid->getConfig()->isPureJs() ) {
	$onchange = 'onchange="AGrid.filterRows(this)"';
}
else if (method_exists($cfg, 'isSubmittedOnChange') && $cfg->isSubmittedOnChange()) {
	if($filter->grid->getConfig()->isAjax() ) {
		$onchange = 'onchange="AGrid.submitAjax(this)"';
	}
	else {
		$onchange = 'onchange="this.form.submit()"';
	}
}


$arrOptions	= array(
	'checked'		=> 'nur ausgewählte',
	'unchecked'		=> 'nur unausgewählte'
);

?>
<select
	style="width: 40px;"
	class="form-control input-sm filter-selected"
	name="<?= $filter->getInputName() ?><?= $cfg->isMultipleMode() ? '[]' : '' ?>"
	<?= $onchange ?>
	<?= ($size = $cfg->getSize()) ? 'size="'.$size.'"' : '' ?>
	<?= ($cfg->isMultipleMode()) ? 'multiple="multiple"' : '' ?>
>
	<?= (!$cfg->isMultipleMode()) ? '<option value="">--//--</option>' : '' ?>
	<?php foreach ($arrOptions as $value => $label): ?>
		<?php
		$maybe_selected = (
			(
				(is_array($filter->getValue()) && in_array($value, $filter->getValue())) ||
				$filter->getValue() == $value
			)
			&& $filter->getValue() !== ''
			&& $filter->getValue() !== null
		) ? 'selected="selected"' : ''
		?>
		<option <?= $maybe_selected ?> value="<?= $value ?>">
			<?= $label ?>
		</option>
	<?php endforeach ?>
</select>
