<fieldset class="agrid-form" onsubmit="event.preventDefault();AGrid.submitAjax(this);" style="display: flex;flex-flow: row column;" data-grid-name="<?= $grid->getConfig()->getName() ?>">

	<?php
	/** @var Nayjest\Grids\DataProvider $data * */
	/** @var Nayjest\Grids\Grid $grid * */
	?>
    <table class="table table-striped" id="<?= $grid->getConfig()->getName() ?>">
		<?= $grid->header() ? $grid->header()->render() : '' ?>
		<?php # ========== TABLE BODY ========== ?>
        <tbody>

		<?php while( $row = $data->getRow() ): ?>
			<?= $grid->getConfig()->getRowComponent()->setDataRow( $row )->render() ?>
		<?php endwhile; ?>

        </tbody>
		<?= $grid->footer() ? $grid->footer()->render() : '' ?>
    </table>
	<?php # Hidden input for submitting form by pressing enter if there are no other submits ?>
    <input type="submit" style="display: none;"/>

    <?php foreach( $grid->getConfig()->getAjaxParams() as $key => $value ): ?>
        <input type="hidden" name="<?= $key ?>" value="<?= $value ?>" >
    <?php endforeach; ?>

    <script>
		AGrid.addGrid('<?= $grid->getConfig()->getName() ?>')
    </script>
</fieldset>



