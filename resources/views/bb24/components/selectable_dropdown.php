<?php
//	dd( $config->getSelectedEntities() );
use Bb24\Agrid\Interfaces\EntitySelect; ?>

<div class="btn-group">
	<input type="hidden" class="entity-select-mode" name="<?= $config->getName() ?>[entity-select-mode]" value="<?= $config->getSelectMode() ?>" >
    <input type="hidden" class="entity-select-ids"  name="<?= $grid->getConfig()->getName(); ?>[selected-entities]" value="<?= $config->getSelectedEntitiesAsString() ?>" >
	<button class="btn btn-primary dropdown-toggle select-entity-button" type="button" data-toggle="dropdown" aria-haspopup="true" data-total-entities="<?= $config->getDataProvider()->getPaginator()->total() ?>" aria-expanded="false">Auswählen (<?= $config->getSelectedCount() ?>)</button>
	<div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
        
        <?php if( $config->getSelectableConfig()->isRenderSelectAll() ): ?>
		    <a class="dropdown-item" href="#" onclick="AGrid.onSelectModeChange(this);" data-select-mode="<?= EntitySelect::SELECT_ALL ?>" >Alles auswählen</a>
        <?php endif; ?>

		<?php if( $config->getSelectableConfig()->isRenderSelectCurrent() ): ?>
		    <a class="dropdown-item" href="#" onclick="AGrid.onSelectModeChange(this);" data-select-mode="<?= EntitySelect::SELECT_CURRENT ?>" >Aktuelle Seite auswählen</a>
        <?php endif; ?>

		<?php if( $config->getSelectableConfig()->isRenderSelectCurrent() || $config->getSelectableConfig()->isRenderSelectAll() ): ?>
		    <div class="dropdown-divider"></div>
        <?php endif; ?>
		<a class="dropdown-item" href="#" onclick="AGrid.onSelectModeChange(this);" data-select-mode="<?= EntitySelect::SELECT_NOTHING ?>" >Nichts auswählen</a>
	</div>
</div>