<small style="white-space: nowrap">
    <?php  dd( $grid ); ?>
    <a
		title="Sort ascending"
		<?php if($column->isSortedAsc()): ?>
			class="text-success"
		<?php else: ?>
            href="#"
            onclick="event.preventDefault();AGrid.sort( this, '<?= $column->getName() ?>', 'ASC' );AGrid.submit( this );"
		<?php endif ?>
	>
		&#x25B2;
	</a>
	<a
		title="Sort descending"
		<?php if($column->isSortedDesc()): ?>
			class="text-success"
		<?php else: ?>
            href="#"
            onclick="event.preventDefault();AGrid.sort( this, '<?= $column->getName() ?>', 'DESC' );AGrid.submit( this );"
		<?php endif ?>
	>
		&#x25BC;
	</a>
</small>
