<small style="white-space: nowrap">
    <?php if( !$grid->getConfig()->isPureJs() ): ?>
        <a
                title="Sort ascending"
                href="#"
			<?php if($column->isSortedAsc()): ?>
                class="text-success"
			<?php elseif( $grid->getConfig()->isAjax() ): ?>
                onclick="AGrid.submitAjax(this, {'sort-col': '<?= $column->getName() ?>', 'sort-dir': 'ASC'});"
			<?php else: ?>
                onclick="event.preventDefault();AGrid.sort( this, '<?= $column->getName() ?>', 'ASC' );AGrid.submit( this );"
			<?php endif ?>
        >
            &#x25B2;
        </a>
        <a
                href="#"
                title="Sort descending"
			<?php if($column->isSortedDesc()): ?>
                class="text-success"
			<?php elseif( $grid->getConfig()->isAjax() ): ?>
                onclick="AGrid.submitAjax(this, {'sort-col': '<?= $column->getName() ?>', 'sort-dir': 'DESC'});"
			<?php else: ?>
                onclick="event.preventDefault();AGrid.sort( this, '<?= $column->getName() ?>', 'DESC' );AGrid.submit( this );"
			<?php endif ?>
        >
            &#x25BC;
        </a>
    <?php endif; ?>
</small>
